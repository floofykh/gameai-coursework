﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetworks
{
    class Neuron
    {
        private IActivationFunction m_activationFunction = null;
        public IActivationFunction ActivationFunction { get { return m_activationFunction; } set { m_activationFunction = value; } }

        public double Input { get { return Output; } set { Output = value; } }
        public double Output { get; set; }
        public double Error { get; set; }
        public double Target { get; set; }

        private double m_activation = 0d;
        public bool WithinThreshold { get { return Math.Abs(Error) < MultilayerPerceptron.ErrorStopThreshold; } }

        public static double LearningRate { get; set; }
        public static double MomentumAlpha { get; set; }

        public List<double> m_weights;
        private List<double> m_deltaWeights;

        public Neuron()
        {
            Output = 0d;
            m_weights = new List<double>();
            m_deltaWeights = new List<double>();
            m_activationFunction = new TanHActFunction();
        }

        public Neuron(double input)
        {
            Input = input;
            Output = input;
            m_weights = new List<double>();
            m_deltaWeights = new List<double>();
            m_activationFunction = new TanHActFunction();
        }

        public void Initialise(int numWeights)
        {
            for(int i=0; i<numWeights; i++)
            {
                m_weights.Add(Program.rand.NextDouble()*2d - 1d);
            }
        }

        public double Weight(int index)
        {
            if (m_weights != null && m_weights.Count > index)
                return m_weights[index];

            return 0d;
        }

        public void Feed(NeuronLayer layer, int neuronIndex)
        {
            List<Neuron> inputNeurons = layer.Neurons;
            m_activation = 0;

            for (int j = 0; j < layer.NumNeurons; j++)
            {
                m_activation += inputNeurons[j].Output * inputNeurons[j].Weight(neuronIndex);
            }

            Output = m_activationFunction.Function(m_activation);
        }

        public void CalculateError(NeuronLayer successor, bool outputLayer)
        {
            if(outputLayer)
            {
                Error = (Target - Output) * ActivationFunction.FunctionDeriv(Output);
            }
            else
            {
                Error = 0d;
                for(int i=0; i<successor.NumNeurons; i++)
                {
                    Neuron neuron = successor.Neurons[i];
                    Error += (neuron.Error * m_weights[i] * ActivationFunction.FunctionDeriv(Output));
                }
            }
        }

        public void UpdateWeights(NeuronLayer successor)
        {
            if (MomentumAlpha != 0)
            {
                for (int i = 0; i < successor.NumNeurons; i++)
                {
                    var neuron = successor.Neurons[i];

                    if (m_deltaWeights.Count <= i)
                    {
                        double deltaWeight = LearningRate * neuron.Error * Output;

                        m_weights[i] += deltaWeight;
                        m_deltaWeights.Add(deltaWeight);
                    }
                    else
                    {
                        double deltaWeight = /*(1 - MomentumAlpha)*/LearningRate * neuron.Error * Output + MomentumAlpha * m_deltaWeights[i];

                        m_weights[i] += deltaWeight;
                        m_deltaWeights[i] = deltaWeight;
                    }
                }
            }
            else
            {
                for (int i = 0; i < successor.NumNeurons; i++)
                {
                    var neuron = successor.Neurons[i];
                    double deltaWeight = LearningRate * neuron.Error * Output;
                    m_weights[i] += deltaWeight;
                }
            }
        }
    }
}
