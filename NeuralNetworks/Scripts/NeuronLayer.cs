﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetworks
{
    class NeuronLayer
    {
        public int NumNeurons { get { return Neurons.Count; } }
        public List<Neuron> Neurons { get; set; }

        public NeuronLayer ()
        {
            Neurons = new List<Neuron>();
        }

        public void SetInputs(List<double> inputs, bool skipBias)
        {
            for (int i = 0; i < Neurons.Count; i++)
            {
                if(skipBias)
                {
                    if (i != 0)
                        Neurons[i].Input = inputs[i-1];
                }
                else
                {
                    Neurons[i].Input = inputs[i];
                }
            }
        }

        public void SetTargets(List<double> targets)
        {
            for (int i = 0; i < Neurons.Count; i++)
            {
                Neurons[i].Target = targets[i];
            }
        }
    }
}
