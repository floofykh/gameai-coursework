﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetworks
{
    class RadialBasisFunction : IActivationFunction
    {
        private double m_centre = 0d, m_width = 0d;

        public RadialBasisFunction(double centre, double width)
        {
            m_centre = centre;
            m_width = width;
        }

        double IActivationFunction.Function(double activation)
        {
            double dist = activation - m_centre;
            return Math.Exp(-(dist * dist) / (2 * m_width * m_width));
            //return Math.Exp(-Math.Pow(dist / (2 * m_width), 2d));
            //return Math.Exp(-Math.Pow(dist, 2d));
        }

        double IActivationFunction.FunctionDeriv(double activation)
        {
            return 0d;
        }
    }
}
