﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetworks
{
    interface IActivationFunction
    {
        double Function(double activation);
        double FunctionDeriv(double activation);
    }
}
