﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetworks.Scripts.Activation_Functions
{
    class ExponentialActivationFunction : IActivationFunction
    {
        double IActivationFunction.Function(double activation)
        {
            return Math.Exp(activation);
        }

        double IActivationFunction.FunctionDeriv(double activation)
        {
            return Math.Exp(activation);
        }
    }
}
