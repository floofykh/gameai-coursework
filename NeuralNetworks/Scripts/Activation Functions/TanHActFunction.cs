﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetworks
{
    class TanHActFunction : IActivationFunction
    {
        public double Function(double activation)
        {
            return Math.Tanh(activation);
        }

        public double FunctionDeriv(double activation)
        {
            return (1 - (Function(activation) * Function(activation)));
        }
    }
}
