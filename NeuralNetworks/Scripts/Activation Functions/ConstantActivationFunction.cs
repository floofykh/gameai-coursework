﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetworks
{
    class ConstantActivationFunction : IActivationFunction
    {
        double IActivationFunction.Function(double activation)
        {
            return 1d;
        }

        double IActivationFunction.FunctionDeriv(double activation)
        {
            return 1d;
        }
    }
}
