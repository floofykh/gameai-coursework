﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetworks
{
    class MultilayerPerceptron
    {
        private NeuronLayer m_inputLayer;
        private List<NeuronLayer> m_hiddenLayers;
        private NeuronLayer m_outputLayer;

        private int m_numHiddenNeurons = 0;
        public static bool Running = false;

        public static double ErrorStopThreshold {get; set;}
        private static int m_epoch = 0;
        public static int Epoch { get { return m_epoch; } }

        private MLPForm m_MLPForm = null;

        public MultilayerPerceptron(MLPForm MLPForm)
        {
            m_MLPForm = MLPForm;
            m_inputLayer = new NeuronLayer();
            m_hiddenLayers = new List<NeuronLayer>();
            m_outputLayer = new NeuronLayer();
        }

        public void Initialise(int numInputs, int hiddenLayers, int hiddenNeurons, int numOutputs)
        {
            ErrorStopThreshold = 0d;
            m_epoch = 0;

            m_inputLayer.Neurons.Clear();
            //Add bias neuron
            Neuron biasNeuron = new Neuron(1d);
            biasNeuron.Initialise(hiddenNeurons);
            m_inputLayer.Neurons.Add(biasNeuron);

            for(int i=0; i<numInputs; i++)
            {
                Neuron newNeuron = new Neuron();
                newNeuron.Initialise(hiddenNeurons);
                m_inputLayer.Neurons.Add(newNeuron);
            }

            m_outputLayer.Neurons.Clear();
            for (int i = 0; i < numOutputs; i++)
            {
                Neuron newNeuron = new Neuron();
                m_outputLayer.Neurons.Add(newNeuron);
            }

            m_numHiddenNeurons = hiddenNeurons;
            m_hiddenLayers.Clear();
            for (int i = 0; i < hiddenLayers; i++)
            {
                NeuronLayer hiddenLayer = new NeuronLayer();

                for (int j = 0; j < hiddenNeurons; j++)
                {
                    Neuron newNeuron = new Neuron();
                    if (i < hiddenLayers - 1)
                        newNeuron.Initialise(hiddenNeurons);
                    else
                        newNeuron.Initialise(numOutputs);

                    hiddenLayer.Neurons.Add(newNeuron);
                }

                m_hiddenLayers.Add(hiddenLayer);
            }
        }

        public void Train(List<List<double>> inputs, List<List<double>> targets)
        {
            Running = true;
            BackgroundWorker bw = new BackgroundWorker();

            bw.DoWork += new DoWorkEventHandler(
                delegate(object o, DoWorkEventArgs args)
                {
                    while (Running)
                    {
                        TrainPatterns(inputs, targets);

                        m_MLPForm.SetNumEpochsAsync(m_epoch);
                        m_MLPForm.TestAndPresentAsync();
                    }
                });

            bw.RunWorkerAsync();
        }

        private void TrainPatterns(List<List<double>> inputs, List<List<double>> targets)
        {
            Queue<int> randomIndices = GenRandomNonRepNumbers(inputs.Count, 0, inputs.Count, Program.rand);

            bool trained = true;
            while(randomIndices.Count > 0)
            {
                int index = randomIndices.Dequeue();
                TrainPattern(inputs[index], targets[index]);
                foreach(Neuron neuron in m_outputLayer.Neurons)
                {
                    if (!neuron.WithinThreshold)
                        trained = false;
                }
            }
            m_epoch++;

            if (trained)
                Running = false;
        }

        public void TrainPattern(List<double> inputs, List<double> targets)
        {
            InitialisePatternSet(inputs, targets);
            FeedForward();
            FeedBack();
        }

        public List<double> Test(List<double> inputs)
        {
            InitialisePatternSet(inputs, null);
            FeedForward();
            return GetOutput();
        }

        public void FeedForward()
        {
            //Feed from input
            for(int i=0; i<m_hiddenLayers[0].NumNeurons; i++)
            {
                Neuron neuron = m_hiddenLayers[0].Neurons[i];
                neuron.Feed(m_inputLayer, i);
            }

            //Feed through hidden layers
            for(int i=0; i<m_hiddenLayers.Count-1; i++)
            {
                for(int j=0; j<m_hiddenLayers[i+1].NumNeurons;j++)
                {
                    Neuron neuron = m_hiddenLayers[i+1].Neurons[j];
                    neuron.Feed(m_hiddenLayers[i], j);
                }
            }

            //Feed to output
            for (int i = 0; i < m_outputLayer.NumNeurons; i++)
            {
                Neuron neuron = m_outputLayer.Neurons[i];
                neuron.Feed(m_hiddenLayers[m_hiddenLayers.Count-1], i);
            }
        }

        public void FeedBack()
        {
            bool trained = true;
            //Calculate error on output layer
            for(int i=0; i<m_outputLayer.NumNeurons; i++)
            {
                Neuron neuron = m_outputLayer.Neurons[i];
                neuron.CalculateError(null, true);
                if (!neuron.WithinThreshold)
                    trained = false;
            }

            if(!trained)
            {
                //Calculate error and update weights from output layer to last hidden layer
                for (int i = 0; i < m_hiddenLayers[m_hiddenLayers.Count - 1].NumNeurons; i++)
                {
                    Neuron neuron = m_hiddenLayers[m_hiddenLayers.Count - 1].Neurons[i];
                    neuron.CalculateError(m_outputLayer, false);
                    neuron.UpdateWeights(m_outputLayer);
                }

                //Calculate error and update weights on other hidden layers
                for (int i = m_hiddenLayers.Count - 2; i >= 0; i--)
                {
                    for (int j = 0; j < m_hiddenLayers[i].NumNeurons; j++)
                    {
                        Neuron neuron = m_hiddenLayers[i].Neurons[j];
                        neuron.CalculateError(m_hiddenLayers[i + 1], false);
                        neuron.UpdateWeights(m_hiddenLayers[i + 1]);
                    }
                }

                //Update weights on input layer
                for (int i = 0; i < m_inputLayer.NumNeurons; i++)
                {
                    Neuron neuron = m_inputLayer.Neurons[i];
                    neuron.UpdateWeights(m_hiddenLayers[0]);
                }
            }
        }

        public List<double> GetOutput()
        {
            List<double> output = new List<double>();
            for (int i = 0; i < m_outputLayer.NumNeurons; i++)
            {
                output.Add(m_outputLayer.Neurons[i].Output);
            }
            return output;
        }

        private void InitialisePatternSet(List<double> inputs, List<double> targets)
        {
            m_inputLayer.SetInputs(inputs, true);

            if(targets != null)
            {
                m_outputLayer.SetTargets(targets);
            }
        }

        private Queue<int> GenRandomNonRepNumbers(int num, int min, int max, Random generator)
        {
            if (max - min < num)
                return null;

            Queue<int> numbers = new Queue<int>(num);

            for (int i = 0; i < num; i++)
            {
                int randNum = 0;
                do
                {
                    randNum = generator.Next(min, max);
                } while (numbers.Contains(randNum));

                numbers.Enqueue(randNum);
            }

            return numbers;
        }
    }
}
