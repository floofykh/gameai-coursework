﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace NeuralNetworks
{
    public struct RadialBasisFunctionData
    {
        public double Centre, Width;
    }
    class RadialBasisFunctionNetwork
    {

        private NeuronLayer m_inputLayer;
        private NeuronLayer m_radialFunctions;
        private NeuronLayer m_outputLayer;

        private int m_numRadialFunctions = 0;
        public static bool Running = false;

        public static double ErrorStopThreshold {get; set;}
        private static int m_epoch = 0;
        public static int Epoch { get { return m_epoch; } }

        private RBFForm m_RBFForm = null;

        public RadialBasisFunctionNetwork(RBFForm RBFForm)
        {
            m_RBFForm = RBFForm;
            m_inputLayer = new NeuronLayer();
            m_radialFunctions = new NeuronLayer();
            m_outputLayer = new NeuronLayer();
        }

        public void Initialise(int numInputs, List<RadialBasisFunctionData> radialFunctions, int numOutputs)
        {
            ErrorStopThreshold = 0d;
            m_epoch = 0;
            m_numRadialFunctions = radialFunctions.Count;

            m_inputLayer.Neurons.Clear();
            //Add bias neuron
            /*Neuron inputBiasNeuron = new Neuron(1d);
            inputBiasNeuron.Initialise(m_numRadialFunctions);
            m_inputLayer.Neurons.Add(inputBiasNeuron);*/
            for(int i=0; i<numInputs; i++)
            {
                Neuron newNeuron = new Neuron();
                newNeuron.Initialise(m_numRadialFunctions);
                m_inputLayer.Neurons.Add(newNeuron);
            }

            m_outputLayer.Neurons.Clear();
            for (int i = 0; i < numOutputs; i++)
            {
                Neuron newNeuron = new Neuron();
                m_outputLayer.Neurons.Add(newNeuron);
            }

            m_radialFunctions.Neurons.Clear();
            //Add bias neuron
           /* Neuron outputBiasNeuron = new Neuron(1d);
            outputBiasNeuron.Initialise(numOutputs);
            outputBiasNeuron.ActivationFunction = new ConstantActivationFunction();
            m_radialFunctions.Neurons.Add(outputBiasNeuron);*/
            for (int i = 0; i < m_numRadialFunctions; i++)
            {
                Neuron newNeuron = new Neuron();
                newNeuron.Initialise(numOutputs);
                newNeuron.ActivationFunction = new RadialBasisFunction(radialFunctions[i].Centre, radialFunctions[i].Width);
                m_radialFunctions.Neurons.Add(newNeuron);
            }
        }

        public void Train(List<List<double>> inputs, List<List<double>> targets)
        {
            Running = true;
            BackgroundWorker bw = new BackgroundWorker();

            bw.DoWork += new DoWorkEventHandler(
                delegate(object o, DoWorkEventArgs args)
                {
                    while (Running)
                    {
                        TrainPatterns(inputs, targets);

                        m_RBFForm.SetNumEpochsAsync(m_epoch);
                        m_RBFForm.TestAndPresentAsync();
                    }
                });

            bw.RunWorkerAsync();
        }

        private void TrainPatterns(List<List<double>> inputs, List<List<double>> targets)
        {
            Queue<int> randomIndices = GenRandomNonRepNumbers(inputs.Count, 0, inputs.Count, Program.rand);

            bool trained = true;
            while(randomIndices.Count > 0)
            {
                int index = randomIndices.Dequeue();
                TrainPattern(inputs[index], targets[index]);
                foreach(Neuron neuron in m_outputLayer.Neurons)
                {
                    if (Math.Abs(neuron.Error) > ErrorStopThreshold)
                        trained = false;
                }
            }
            m_epoch++;

            if (trained)
                Running = false;
        }

        public void TrainPattern(List<double> inputs, List<double> targets)
        {
            InitialisePatternSet(inputs, targets);
            FeedForward();

            //Update weights
            for (int i = 0; i < m_radialFunctions.NumNeurons; i++)
            {
                Neuron radialFunctionNeuron = m_radialFunctions.Neurons[i];
                for (int j = 0; j < m_outputLayer.NumNeurons; j++)
                {
                    Neuron outputNeuron = m_outputLayer.Neurons[j];
                    if (Math.Abs(outputNeuron.Error) > m_RBFForm.StopThreshold)
                        radialFunctionNeuron.m_weights[j] += Neuron.LearningRate * outputNeuron.Error * radialFunctionNeuron.Output;
                }
            }
        }

        public List<double> Test(List<double> inputs)
        {
            InitialisePatternSet(inputs, null);
            FeedForward();
            return GetOutput();
        }

        public void FeedForward()
        {
            //Feed from input
            for(int i=0; i<m_radialFunctions.NumNeurons; i++)
            {
                Neuron radialFunctionNeuron = m_radialFunctions.Neurons[i];
                radialFunctionNeuron.Output = 0d;
                for(int j=0; j<m_inputLayer.NumNeurons; j++)
                {
                    Neuron inputNeuron = m_inputLayer.Neurons[j];
                    radialFunctionNeuron.Output += radialFunctionNeuron.ActivationFunction.Function(inputNeuron.Output);
                }
            }

            //Feed to output
            for (int i = 0; i < m_outputLayer.NumNeurons; i++)
            {
                Neuron outputNeuron = m_outputLayer.Neurons[i];
                outputNeuron.Output = 0d;
                for (int j = 0; j < m_radialFunctions.NumNeurons; j++)
                {
                    Neuron radialFunctionNeuron = m_radialFunctions.Neurons[j];
                    outputNeuron.Output += radialFunctionNeuron.Weight(i) * radialFunctionNeuron.Output;
                }
                outputNeuron.Error = (outputNeuron.Target - outputNeuron.Output);
            }
        }

        public List<double> GetOutput()
        {
            List<double> output = new List<double>();
            for (int i = 0; i < m_outputLayer.NumNeurons; i++)
            {
                output.Add(m_outputLayer.Neurons[i].Output);
            }
            return output;
        }

        private void InitialisePatternSet(List<double> inputs, List<double> targets)
        {
            m_inputLayer.SetInputs(inputs, false);

            if(targets != null)
            {
                m_outputLayer.SetTargets(targets);
            }
        }

        private Queue<int> GenRandomNonRepNumbers(int num, int min, int max, Random generator)
        {
            if (max - min < num)
                return null;

            Queue<int> numbers = new Queue<int>(num);

            for (int i = 0; i < num; i++)
            {
                int randNum = 0;
                do
                {
                    randNum = generator.Next(min, max);
                } while (numbers.Contains(randNum));

                numbers.Enqueue(randNum);
            }

            return numbers;
        }
    }
}