﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace NeuralNetworks
{
    public partial class MLPForm : Form
    {
        private const double X_MAX = Math.PI * 2;
        private const double X_MIN = 0;
        private const double INTERVAL = Math.PI / 90d;
        private double m_numPlotPoints;
        private double m_noiseValue = 0;
        private double m_stopThreshold;
        private string m_function = "";
        private List<double> m_inputs, m_targets;
        private MultilayerPerceptron m_mlp = null;
        private int m_numHiddenLayers = 1, m_numHiddenNeurons = 1;
        private bool m_changed = true;
        private const int testCases = 180;

        public MLPForm()
        {
            InitializeComponent();
            m_graph.ChartAreas.Add("function");

            m_graph.Series.Add("Neural Network");
            m_graph.Series.Add("Function");
            m_graph.Series.Add("Points");

            Neuron.LearningRate = ((double)(m_learningRateSelector).Value);
            m_numHiddenLayers = ((int)(m_numHiddenLayersInput).Value);
            m_numHiddenNeurons = ((int)(m_numHiddenNeuronsSelector).Value);

            m_mlp = new MultilayerPerceptron(this);
        }
        private void InitialiseFunctionGraph()
        {
            Series func = m_graph.Series.FindByName("Function");
            func.Points.Clear();
            func.ChartType = SeriesChartType.Line;
            func.Color = Color.Green;
            func.BorderWidth = 1;

            for (double x = X_MIN; x < X_MAX; x += INTERVAL)
            {
                double y = 0;
                switch (m_function)
                {
                    case "Sin":
                        y = Math.Sin(x);
                        break;
                    case "Cos":
                        y = Math.Cos(x);
                        break;
                };
                func.Points.AddXY(x, y);
            }
        }

        private void InitialiseInputPoints()
        {
            m_inputs = new List<double>();
            m_targets = new List<double>();

            Series points = m_graph.Series.FindByName("Points");
            points.Points.Clear();
            points.ChartType = SeriesChartType.Point;
            points.Color = Color.Blue;
            points.BorderWidth = 1;

            double interval = 0d;
            if(m_numPlotPoints > 1)
                interval = (X_MAX-X_MIN)/(m_numPlotPoints-1);

            for (int point = 0; point < m_numPlotPoints; point++)
            {
                double x = X_MIN + point * interval;
                double y = 0;
                switch (m_function)
                {
                    case "Sin":
                        y = Math.Sin(x);
                        break;
                    case "Cos":
                        y = Math.Cos(x);
                        break;
                };

                y += (Program.rand.NextDouble() - 0.5d) * 2d * m_noiseValue;
                m_targets.Add(y);
                m_inputs.Add(x);
                points.Points.AddXY(x, y);
            }
        }

        public void TestAndPresent()
        {
            List<double> finalData = new List<double>();

            for (double x = X_MIN; x < X_MAX; x += INTERVAL)
            {
                List<double> input = new List<double>();
                input.Add(x);

                finalData.AddRange(m_mlp.Test(input));
            }
            PlotNeuralOutput(finalData);
        }
        public void TestAndPresentAsync()
        {
            List<double> finalData = new List<double>();

            for (double x = X_MIN; x < X_MAX; x += INTERVAL)
            {
                List<double> input = new List<double>();
                input.Add(x);

                finalData.AddRange(m_mlp.Test(input));
            }
            PlotNeuralOutputAsync(finalData);
        }

        private void m_trainButton_Click(object sender, EventArgs e)
        {
            if (m_mlp != null)
            {
                if(MultilayerPerceptron.Running)
                {
                    MultilayerPerceptron.Running = false;
                    m_trainButton.Text = "Train";
                }
                else
                {
                    m_trainButton.Text = "Stop";
                    if (m_changed)
                    {
                        m_mlp.Initialise(1, m_numHiddenLayers, m_numHiddenNeurons, 1);
                        m_changed = false;
                    }

                    MultilayerPerceptron.ErrorStopThreshold = m_stopThreshold;
                    List<List<double>> inputPatterns = new List<List<double>>();
                    List<List<double>> targetPatterns = new List<List<double>>();

                    for (int i = 0; i < m_inputs.Count; i++)
                    {
                        List<double> newInputPattern = new List<double>();
                        newInputPattern.Add(m_inputs[i]);
                        List<double> newTargetPattern = new List<double>();
                        newTargetPattern.Add(m_targets[i]);

                        inputPatterns.Add(newInputPattern);
                        targetPatterns.Add(newTargetPattern);

                    }

                    m_mlp.Train(inputPatterns, targetPatterns);
                }
            }
        }
        public void PlotNeuralOutput(List<double> output)
        {
            Series network = m_graph.Series["Neural Network"];

            network.Points.Clear();
            network.ChartType = SeriesChartType.Line;
            network.Color = Color.Red;
            network.BorderWidth = 3;

            double x = 0;
            for (int i = 0; i < output.Count; i++)
            {
                network.Points.AddXY(x, output[i]);
                x += INTERVAL;
            }
        }
        public void PlotNeuralOutputAsync(List<double> output)
        {
            try
            {
                if (m_graph.InvokeRequired)
                {
                    m_graph.Invoke((MethodInvoker)delegate
                    {
                        Series network = m_graph.Series["Neural Network"];

                        network.Points.Clear();
                        network.ChartType = SeriesChartType.Line;
                        network.Color = Color.Red;
                        network.BorderWidth = 3;

                        double x = 0;
                        for (int i = 0; i < output.Count; i++)
                        {
                            network.Points.AddXY(x, output[i]);
                            x += INTERVAL;
                        }
                    });
                }
            }
            catch (Exception) { }
        }

        private void m_numHiddenLayersInput_ValueChanged(object sender, EventArgs e)
        {
            int value = ((int)((NumericUpDown)sender).Value);
            m_numHiddenLayers = value;
            m_changed = true;
        }

        private void m_functionSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_function = ((ComboBox)sender).SelectedItem.ToString();
            InitialiseFunctionGraph();
            m_changed = true;
        }

        private void m_numHiddenNeuronsSelector_ValueChanged(object sender, EventArgs e)
        {
            int value = ((int)((NumericUpDown)sender).Value);
            m_numHiddenNeurons = value;
            m_changed = true;
        }

        private void m_learningRateSelector_ValueChanged(object sender, EventArgs e)
        {
            double value = ((double)((NumericUpDown)sender).Value);
            Neuron.LearningRate = value;
            m_changed = true;
        }

        private void m_momentumController_ValueChanged(object sender, EventArgs e)
        {
            double value = ((double)((NumericUpDown)sender).Value);
            Neuron.MomentumAlpha = value;
            m_changed = true;
        }

        private void m_noiseController_ValueChanged(object sender, EventArgs e)
        {
            double value = ((double)((NumericUpDown)sender).Value);
            m_noiseValue = value;
            InitialiseInputPoints();
            m_changed = true;
        }

        private void m_plotPointsController_ValueChanged(object sender, EventArgs e)
        {
            double value = ((double)((NumericUpDown)sender).Value);
            m_numPlotPoints = value;
            InitialiseInputPoints();
            m_changed = true;
        }

        public void SetNumEpochs(int num)
        {
            m_numEpochLabel.Text = num.ToString();
        }

        public void SetNumEpochsAsync(int num)
        {
            try
            {
                if (m_numEpochLabel.InvokeRequired)
                {
                    m_numEpochLabel.Invoke((MethodInvoker)delegate
                    {
                        m_numEpochLabel.Text = num.ToString();
                    });
                }
            }
            catch (Exception) { }
        }

        private void thresholdController_ValueChanged(object sender, EventArgs e)
        {
            double value = ((double)((NumericUpDown)sender).Value);
            m_stopThreshold = value;
            m_changed = true;
        }
    }
}
