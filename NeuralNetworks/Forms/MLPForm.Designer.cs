﻿namespace NeuralNetworks
{
    partial class MLPForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_graph = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.m_numHiddenLayersInput = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.m_trainButton = new System.Windows.Forms.Button();
            this.m_functionSelector = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.m_numHiddenNeuronsSelector = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.m_learningRateSelector = new System.Windows.Forms.NumericUpDown();
            this.m_noiseController = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.m_momentumController = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.m_plotPointsController = new System.Windows.Forms.NumericUpDown();
            this.m_numEpochLabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.m_thresholdController = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.m_graph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_numHiddenLayersInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_numHiddenNeuronsSelector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_learningRateSelector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_noiseController)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_momentumController)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_plotPointsController)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_thresholdController)).BeginInit();
            this.SuspendLayout();
            // 
            // m_graph
            // 
            this.m_graph.Location = new System.Drawing.Point(0, 0);
            this.m_graph.Name = "m_graph";
            this.m_graph.Size = new System.Drawing.Size(784, 400);
            this.m_graph.TabIndex = 0;
            this.m_graph.Text = "chart1";
            // 
            // m_numHiddenLayersInput
            // 
            this.m_numHiddenLayersInput.Location = new System.Drawing.Point(163, 428);
            this.m_numHiddenLayersInput.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.m_numHiddenLayersInput.Name = "m_numHiddenLayersInput";
            this.m_numHiddenLayersInput.Size = new System.Drawing.Size(120, 20);
            this.m_numHiddenLayersInput.TabIndex = 1;
            this.m_numHiddenLayersInput.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.m_numHiddenLayersInput.ValueChanged += new System.EventHandler(this.m_numHiddenLayersInput_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 430);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Hidden Layers";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(398, 532);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Number of Epochs:";
            // 
            // m_trainButton
            // 
            this.m_trainButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_trainButton.Location = new System.Drawing.Point(646, 456);
            this.m_trainButton.Name = "m_trainButton";
            this.m_trainButton.Size = new System.Drawing.Size(126, 62);
            this.m_trainButton.TabIndex = 5;
            this.m_trainButton.Text = "Train";
            this.m_trainButton.UseVisualStyleBackColor = true;
            this.m_trainButton.Click += new System.EventHandler(this.m_trainButton_Click);
            // 
            // m_functionSelector
            // 
            this.m_functionSelector.FormattingEnabled = true;
            this.m_functionSelector.Items.AddRange(new object[] {
            "Sin",
            "Cos"});
            this.m_functionSelector.Location = new System.Drawing.Point(499, 422);
            this.m_functionSelector.Name = "m_functionSelector";
            this.m_functionSelector.Size = new System.Drawing.Size(126, 21);
            this.m_functionSelector.TabIndex = 6;
            this.m_functionSelector.SelectedIndexChanged += new System.EventHandler(this.m_functionSelector_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 482);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Number of Hidden Neurons";
            // 
            // m_numHiddenNeuronsSelector
            // 
            this.m_numHiddenNeuronsSelector.Location = new System.Drawing.Point(163, 480);
            this.m_numHiddenNeuronsSelector.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.m_numHiddenNeuronsSelector.Name = "m_numHiddenNeuronsSelector";
            this.m_numHiddenNeuronsSelector.Size = new System.Drawing.Size(120, 20);
            this.m_numHiddenNeuronsSelector.TabIndex = 8;
            this.m_numHiddenNeuronsSelector.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.m_numHiddenNeuronsSelector.ValueChanged += new System.EventHandler(this.m_numHiddenNeuronsSelector_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 456);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Learning Rate";
            // 
            // m_learningRateSelector
            // 
            this.m_learningRateSelector.DecimalPlaces = 10;
            this.m_learningRateSelector.Increment = new decimal(new int[] {
            1,
            0,
            0,
            655360});
            this.m_learningRateSelector.Location = new System.Drawing.Point(163, 454);
            this.m_learningRateSelector.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.m_learningRateSelector.Name = "m_learningRateSelector";
            this.m_learningRateSelector.Size = new System.Drawing.Size(120, 20);
            this.m_learningRateSelector.TabIndex = 11;
            this.m_learningRateSelector.ValueChanged += new System.EventHandler(this.m_learningRateSelector_ValueChanged);
            // 
            // m_noiseController
            // 
            this.m_noiseController.DecimalPlaces = 2;
            this.m_noiseController.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.m_noiseController.Location = new System.Drawing.Point(499, 493);
            this.m_noiseController.Name = "m_noiseController";
            this.m_noiseController.Size = new System.Drawing.Size(120, 20);
            this.m_noiseController.TabIndex = 12;
            this.m_noiseController.ValueChanged += new System.EventHandler(this.m_noiseController_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(429, 495);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Noise Value";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 508);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Momentum";
            // 
            // m_momentumController
            // 
            this.m_momentumController.DecimalPlaces = 5;
            this.m_momentumController.Increment = new decimal(new int[] {
            1,
            0,
            0,
            327680});
            this.m_momentumController.Location = new System.Drawing.Point(163, 508);
            this.m_momentumController.Name = "m_momentumController";
            this.m_momentumController.Size = new System.Drawing.Size(120, 20);
            this.m_momentumController.TabIndex = 15;
            this.m_momentumController.ValueChanged += new System.EventHandler(this.m_momentumController_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(444, 425);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Function";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(429, 461);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Input Points";
            // 
            // m_plotPointsController
            // 
            this.m_plotPointsController.Location = new System.Drawing.Point(499, 461);
            this.m_plotPointsController.Name = "m_plotPointsController";
            this.m_plotPointsController.Size = new System.Drawing.Size(120, 20);
            this.m_plotPointsController.TabIndex = 19;
            this.m_plotPointsController.ValueChanged += new System.EventHandler(this.m_plotPointsController_ValueChanged);
            // 
            // m_numEpochLabel
            // 
            this.m_numEpochLabel.AutoSize = true;
            this.m_numEpochLabel.Location = new System.Drawing.Point(500, 532);
            this.m_numEpochLabel.Name = "m_numEpochLabel";
            this.m_numEpochLabel.Size = new System.Drawing.Size(13, 13);
            this.m_numEpochLabel.TabIndex = 20;
            this.m_numEpochLabel.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 532);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Stop Threshold";
            // 
            // m_thresholdController
            // 
            this.m_thresholdController.DecimalPlaces = 5;
            this.m_thresholdController.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.m_thresholdController.Location = new System.Drawing.Point(163, 532);
            this.m_thresholdController.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.m_thresholdController.Name = "m_thresholdController";
            this.m_thresholdController.Size = new System.Drawing.Size(120, 20);
            this.m_thresholdController.TabIndex = 22;
            this.m_thresholdController.ValueChanged += new System.EventHandler(this.thresholdController_ValueChanged);
            // 
            // MLPForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.m_thresholdController);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.m_numEpochLabel);
            this.Controls.Add(this.m_plotPointsController);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.m_momentumController);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.m_noiseController);
            this.Controls.Add(this.m_learningRateSelector);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.m_numHiddenNeuronsSelector);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.m_functionSelector);
            this.Controls.Add(this.m_trainButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_numHiddenLayersInput);
            this.Controls.Add(this.m_graph);
            this.Name = "MLPForm";
            this.Text = "MLPForm";
            ((System.ComponentModel.ISupportInitialize)(this.m_graph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_numHiddenLayersInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_numHiddenNeuronsSelector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_learningRateSelector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_noiseController)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_momentumController)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_plotPointsController)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_thresholdController)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart m_graph;
        private System.Windows.Forms.NumericUpDown m_numHiddenLayersInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button m_trainButton;
        private System.Windows.Forms.ComboBox m_functionSelector;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown m_numHiddenNeuronsSelector;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown m_learningRateSelector;
        private System.Windows.Forms.NumericUpDown m_noiseController;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown m_momentumController;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown m_plotPointsController;
        private System.Windows.Forms.Label m_numEpochLabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown m_thresholdController;
    }
}