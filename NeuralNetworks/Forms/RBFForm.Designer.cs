﻿namespace NeuralNetworks
{
    partial class RBFForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.m_functionGraph = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.m_rbfGraph = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label1 = new System.Windows.Forms.Label();
            this.m_numRBFsInput = new System.Windows.Forms.NumericUpDown();
            this.m_thresholdController = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.m_numEpochLabel = new System.Windows.Forms.Label();
            this.m_plotPointsController = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.m_noiseController = new System.Windows.Forms.NumericUpDown();
            this.m_learningRateSelector = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.m_functionSelector = new System.Windows.Forms.ComboBox();
            this.m_trainButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.m_rbfWidthController = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.m_rbfOffsetController = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.m_rbfSeperationController = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.m_functionGraph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_rbfGraph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_numRBFsInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_thresholdController)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_plotPointsController)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_noiseController)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_learningRateSelector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_rbfWidthController)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_rbfOffsetController)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_rbfSeperationController)).BeginInit();
            this.SuspendLayout();
            // 
            // m_functionGraph
            // 
            this.m_functionGraph.Location = new System.Drawing.Point(0, 0);
            this.m_functionGraph.Name = "m_functionGraph";
            this.m_functionGraph.Size = new System.Drawing.Size(784, 305);
            this.m_functionGraph.TabIndex = 0;
            this.m_functionGraph.Text = "m_functionGraph";
            // 
            // m_rbfGraph
            // 
            this.m_rbfGraph.Location = new System.Drawing.Point(0, 311);
            this.m_rbfGraph.Name = "m_rbfGraph";
            series1.Name = "Series1";
            this.m_rbfGraph.Series.Add(series1);
            this.m_rbfGraph.Size = new System.Drawing.Size(784, 99);
            this.m_rbfGraph.TabIndex = 1;
            this.m_rbfGraph.Text = "m_rbfGraph";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 413);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "RBFs";
            // 
            // m_numRBFsInput
            // 
            this.m_numRBFsInput.Location = new System.Drawing.Point(163, 411);
            this.m_numRBFsInput.Name = "m_numRBFsInput";
            this.m_numRBFsInput.Size = new System.Drawing.Size(120, 20);
            this.m_numRBFsInput.TabIndex = 3;
            this.m_numRBFsInput.ValueChanged += new System.EventHandler(this.m_numRBFsInput_ValueChanged);
            // 
            // m_thresholdController
            // 
            this.m_thresholdController.DecimalPlaces = 5;
            this.m_thresholdController.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.m_thresholdController.Location = new System.Drawing.Point(163, 539);
            this.m_thresholdController.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.m_thresholdController.Name = "m_thresholdController";
            this.m_thresholdController.Size = new System.Drawing.Size(120, 20);
            this.m_thresholdController.TabIndex = 40;
            this.m_thresholdController.ValueChanged += new System.EventHandler(this.m_thresholdController_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 539);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 13);
            this.label9.TabIndex = 39;
            this.label9.Text = "Stop Threshold";
            // 
            // m_numEpochLabel
            // 
            this.m_numEpochLabel.AutoSize = true;
            this.m_numEpochLabel.Location = new System.Drawing.Point(501, 536);
            this.m_numEpochLabel.Name = "m_numEpochLabel";
            this.m_numEpochLabel.Size = new System.Drawing.Size(13, 13);
            this.m_numEpochLabel.TabIndex = 38;
            this.m_numEpochLabel.Text = "0";
            // 
            // m_plotPointsController
            // 
            this.m_plotPointsController.Location = new System.Drawing.Point(500, 465);
            this.m_plotPointsController.Name = "m_plotPointsController";
            this.m_plotPointsController.Size = new System.Drawing.Size(120, 20);
            this.m_plotPointsController.TabIndex = 37;
            this.m_plotPointsController.ValueChanged += new System.EventHandler(this.m_plotPointsController_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(430, 465);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 36;
            this.label8.Text = "Input Points";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(445, 429);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 34;
            this.label7.Text = "Function";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(430, 499);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "Noise Value";
            // 
            // m_noiseController
            // 
            this.m_noiseController.DecimalPlaces = 2;
            this.m_noiseController.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.m_noiseController.Location = new System.Drawing.Point(500, 497);
            this.m_noiseController.Name = "m_noiseController";
            this.m_noiseController.Size = new System.Drawing.Size(120, 20);
            this.m_noiseController.TabIndex = 30;
            this.m_noiseController.ValueChanged += new System.EventHandler(this.m_noiseController_ValueChanged);
            // 
            // m_learningRateSelector
            // 
            this.m_learningRateSelector.DecimalPlaces = 10;
            this.m_learningRateSelector.Increment = new decimal(new int[] {
            1,
            0,
            0,
            655360});
            this.m_learningRateSelector.Location = new System.Drawing.Point(163, 513);
            this.m_learningRateSelector.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.m_learningRateSelector.Name = "m_learningRateSelector";
            this.m_learningRateSelector.Size = new System.Drawing.Size(120, 20);
            this.m_learningRateSelector.TabIndex = 29;
            this.m_learningRateSelector.ValueChanged += new System.EventHandler(this.m_learningRateSelector_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 515);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 28;
            this.label4.Text = "Learning Rate";
            // 
            // m_functionSelector
            // 
            this.m_functionSelector.FormattingEnabled = true;
            this.m_functionSelector.Items.AddRange(new object[] {
            "Sin",
            "Cos"});
            this.m_functionSelector.Location = new System.Drawing.Point(500, 426);
            this.m_functionSelector.Name = "m_functionSelector";
            this.m_functionSelector.Size = new System.Drawing.Size(126, 21);
            this.m_functionSelector.TabIndex = 25;
            this.m_functionSelector.SelectedIndexChanged += new System.EventHandler(this.m_functionSelector_SelectedIndexChanged);
            // 
            // m_trainButton
            // 
            this.m_trainButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_trainButton.Location = new System.Drawing.Point(646, 455);
            this.m_trainButton.Name = "m_trainButton";
            this.m_trainButton.Size = new System.Drawing.Size(126, 71);
            this.m_trainButton.TabIndex = 24;
            this.m_trainButton.Text = "Train";
            this.m_trainButton.UseVisualStyleBackColor = true;
            this.m_trainButton.Click += new System.EventHandler(this.m_trainButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(399, 536);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Number of Epochs:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 429);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 13);
            this.label10.TabIndex = 42;
            this.label10.Text = "RBFs";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(163, 427);
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 41;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // m_rbfWidthController
            // 
            this.m_rbfWidthController.DecimalPlaces = 4;
            this.m_rbfWidthController.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.m_rbfWidthController.Location = new System.Drawing.Point(163, 429);
            this.m_rbfWidthController.Name = "m_rbfWidthController";
            this.m_rbfWidthController.Size = new System.Drawing.Size(120, 20);
            this.m_rbfWidthController.TabIndex = 41;
            this.m_rbfWidthController.ValueChanged += new System.EventHandler(this.m_rbfWidthController_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 431);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 13);
            this.label11.TabIndex = 42;
            this.label11.Text = "RBF Width";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 452);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 13);
            this.label12.TabIndex = 44;
            this.label12.Text = "RBF Offset";
            // 
            // m_rbfOffsetController
            // 
            this.m_rbfOffsetController.DecimalPlaces = 2;
            this.m_rbfOffsetController.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.m_rbfOffsetController.Location = new System.Drawing.Point(163, 450);
            this.m_rbfOffsetController.Name = "m_rbfOffsetController";
            this.m_rbfOffsetController.Size = new System.Drawing.Size(120, 20);
            this.m_rbfOffsetController.TabIndex = 43;
            this.m_rbfOffsetController.ValueChanged += new System.EventHandler(this.m_rbfOffsetController_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 472);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(82, 13);
            this.label13.TabIndex = 46;
            this.label13.Text = "RBF Seperation";
            // 
            // m_rbfSeperationController
            // 
            this.m_rbfSeperationController.DecimalPlaces = 2;
            this.m_rbfSeperationController.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.m_rbfSeperationController.Location = new System.Drawing.Point(163, 472);
            this.m_rbfSeperationController.Name = "m_rbfSeperationController";
            this.m_rbfSeperationController.Size = new System.Drawing.Size(67, 20);
            this.m_rbfSeperationController.TabIndex = 45;
            this.m_rbfSeperationController.ValueChanged += new System.EventHandler(this.m_rbfSeperationController_ValueChanged);
            // 
            // RBFForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.m_rbfSeperationController);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.m_rbfOffsetController);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.m_rbfWidthController);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.m_thresholdController);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.m_numEpochLabel);
            this.Controls.Add(this.m_plotPointsController);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.m_noiseController);
            this.Controls.Add(this.m_learningRateSelector);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.m_functionSelector);
            this.Controls.Add(this.m_trainButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_numRBFsInput);
            this.Controls.Add(this.m_rbfGraph);
            this.Controls.Add(this.m_functionGraph);
            this.Name = "RBFForm";
            this.Text = "RBFForm";
            ((System.ComponentModel.ISupportInitialize)(this.m_functionGraph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_rbfGraph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_numRBFsInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_thresholdController)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_plotPointsController)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_noiseController)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_learningRateSelector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_rbfWidthController)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_rbfOffsetController)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_rbfSeperationController)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart m_functionGraph;
        private System.Windows.Forms.DataVisualization.Charting.Chart m_rbfGraph;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown m_numRBFsInput;
        private System.Windows.Forms.NumericUpDown m_thresholdController;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label m_numEpochLabel;
        private System.Windows.Forms.NumericUpDown m_plotPointsController;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown m_noiseController;
        private System.Windows.Forms.NumericUpDown m_learningRateSelector;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox m_functionSelector;
        private System.Windows.Forms.Button m_trainButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.NumericUpDown m_rbfWidthController;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown m_rbfOffsetController;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown m_rbfSeperationController;
    }
}