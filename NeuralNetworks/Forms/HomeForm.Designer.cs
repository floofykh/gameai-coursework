﻿namespace NeuralNetworks
{
    partial class HomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_mlpButton = new System.Windows.Forms.Button();
            this.m_rbfButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_mlpButton
            // 
            this.m_mlpButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_mlpButton.Location = new System.Drawing.Point(47, 222);
            this.m_mlpButton.Name = "m_mlpButton";
            this.m_mlpButton.Size = new System.Drawing.Size(250, 100);
            this.m_mlpButton.TabIndex = 0;
            this.m_mlpButton.Text = "MLP";
            this.m_mlpButton.UseVisualStyleBackColor = true;
            this.m_mlpButton.Click += new System.EventHandler(this.m_mlpButton_Click);
            // 
            // m_rbfButton
            // 
            this.m_rbfButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_rbfButton.Location = new System.Drawing.Point(479, 222);
            this.m_rbfButton.Name = "m_rbfButton";
            this.m_rbfButton.Size = new System.Drawing.Size(250, 100);
            this.m_rbfButton.TabIndex = 1;
            this.m_rbfButton.Text = "RBF";
            this.m_rbfButton.UseVisualStyleBackColor = true;
            this.m_rbfButton.Click += new System.EventHandler(this.m_rbfButton_Click);
            // 
            // HomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.m_rbfButton);
            this.Controls.Add(this.m_mlpButton);
            this.Name = "HomeForm";
            this.Text = "Home";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button m_mlpButton;
        private System.Windows.Forms.Button m_rbfButton;


    }
}

