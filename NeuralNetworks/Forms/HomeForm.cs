﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace NeuralNetworks
{
    public partial class HomeForm : Form
    {
        public HomeForm()
        {
            InitializeComponent();
        }

        private void m_rbfButton_Click(object sender, EventArgs e)
        {
            RBFForm newForm = new RBFForm();
            newForm.Show();
        }

        private void m_mlpButton_Click(object sender, EventArgs e)
        {
            MLPForm newForm = new MLPForm();
            newForm.Show();
        }
    }
}
