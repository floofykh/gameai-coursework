﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace NeuralNetworks
{
    public partial class RBFForm : Form
    {
        private const double X_MAX = Math.PI * 2;
        private const double X_MIN = 0;
        private const double INTERVAL = Math.PI / 90d;
        private double m_numPlotPoints;
        private double m_noiseValue = 0;
        public double StopThreshold { get { return m_stopThreshold; } }
        private double m_stopThreshold;
        private string m_function = "";
        private List<double> m_inputs, m_targets;
        private List<RadialBasisFunctionData> m_rbfsData;
        private RadialBasisFunctionNetwork m_rbf;
        private int m_numRBFs = 1;
        private double m_rbfWidth = 1d, m_rbfOffset = 0d, m_rbfSeperation = 0d;
        private bool m_changed = true;
        private const int testCases = 180;

        public RBFForm()
        {
            InitializeComponent();
            ChartArea functionArea = m_functionGraph.ChartAreas.Add("function");
            functionArea.AxisX.Maximum = X_MAX;
            functionArea.AxisX.Minimum = X_MIN;
            functionArea.AxisY.Maximum = 1.5;
            functionArea.AxisY.Minimum = -1.5;
            ChartArea rbfArea = m_rbfGraph.ChartAreas.Add("RBFs");
            rbfArea.AxisX.Maximum = X_MAX;
            rbfArea.AxisX.Minimum = X_MIN;
            rbfArea.AxisY.Maximum = 1;
            rbfArea.AxisY.Minimum = 0;

            m_functionGraph.Series.Add("Neural Network");
            m_functionGraph.Series.Add("Function");
            m_functionGraph.Series.Add("Points");
            m_rbfGraph.Series.Add("RBFs");

            Neuron.LearningRate = ((double)(m_learningRateSelector).Value);
            m_numRBFs = ((int)(m_numRBFsInput).Value);
            m_rbfOffset = ((double)(m_rbfOffsetController).Value);
            m_rbfSeperation = ((double)(m_rbfOffsetController).Value);
            m_rbfWidth = ((double)(m_rbfWidthController).Value);

            m_rbf = new RadialBasisFunctionNetwork(this);
        }
        private void InitialiseFunctionGraph()
        {
            Series func = m_functionGraph.Series.FindByName("Function");
            func.Points.Clear();
            func.ChartType = SeriesChartType.Line;
            func.Color = Color.Green;
            func.BorderWidth = 1;

            for (double x = X_MIN; x < X_MAX; x += INTERVAL)
            {
                double y = 0;
                switch (m_function)
                {
                    case "Sin":
                        y = Math.Sin(x);
                        break;
                    case "Cos":
                        y = Math.Cos(x);
                        break;
                };
                func.Points.AddXY(x, y);
            }
        }

        private void InitialiseRBFs()
        {
            m_rbfsData = new List<RadialBasisFunctionData>();
            Series rbfs = m_rbfGraph.Series.FindByName("RBFs");
            rbfs.Points.Clear();
            rbfs.ChartType = SeriesChartType.Line;
            rbfs.Color = Color.IndianRed;
            rbfs.BorderWidth = 1;

            for(int i=0; i<m_numRBFs; i++)
            {
                double centre = X_MIN + m_rbfOffset + m_rbfSeperation * i;
                RadialBasisFunctionData data = new RadialBasisFunctionData();
                data.Centre = centre;
                data.Width = m_rbfWidth;
                m_rbfsData.Add(data);
                DrawRBF(centre, m_rbfWidth, rbfs.Points);
            }
        }

        private void DrawRBF(double centre, double width, DataPointCollection points)
        {
            if(width > 0)
            {
                IActivationFunction function = new RadialBasisFunction(centre, width);
                for (double x = X_MIN; x < X_MAX; x += INTERVAL)
                {
                    double y = function.Function(x);
                    points.AddXY(x, y);
                }
            }
        }

        private void InitialiseInputPoints()
        {
            m_inputs = new List<double>();
            m_targets = new List<double>();

            Series points = m_functionGraph.Series.FindByName("Points");
            points.Points.Clear();
            points.ChartType = SeriesChartType.Point;
            points.Color = Color.Blue;
            points.BorderWidth = 1;

            double interval = 0d;
            if (m_numPlotPoints > 1)
                interval = (X_MAX - X_MIN) / (m_numPlotPoints - 1);

            for (int point = 0; point < m_numPlotPoints; point++)
            {
                double x = X_MIN + point * interval;
                double y = 0;
                switch (m_function)
                {
                    case "Sin":
                        y = Math.Sin(x);
                        break;
                    case "Cos":
                        y = Math.Cos(x);
                        break;
                };

                y += (Program.rand.NextDouble() - 0.5d) * 2d * m_noiseValue;
                m_targets.Add(y);
                m_inputs.Add(x);
                points.Points.AddXY(x, y);
            }
        }
        public void SetNumEpochs(int num)
        {
            m_numEpochLabel.Text = num.ToString();
        }

        public void SetNumEpochsAsync(int num)
        {
            try
            {
                if (m_numEpochLabel.InvokeRequired)
                {
                    m_numEpochLabel.Invoke((MethodInvoker)delegate
                    {
                        m_numEpochLabel.Text = num.ToString();
                    });
                }
            }
            catch (Exception) { }
        }

        private void m_rbfSeperationController_ValueChanged(object sender, EventArgs e)
        {
            double value = ((double)((NumericUpDown)sender).Value);
            m_rbfSeperation = value;
            InitialiseRBFs();
            m_changed = true;
        }

        private void m_numRBFsInput_ValueChanged(object sender, EventArgs e)
        {
            int value = ((int)((NumericUpDown)sender).Value);
            m_numRBFs = value;
            InitialiseRBFs();
            m_changed = true;
        }

        private void m_rbfWidthController_ValueChanged(object sender, EventArgs e)
        {
            double value = ((double)((NumericUpDown)sender).Value);
            m_rbfWidth = value;
            InitialiseRBFs();
            m_changed = true;
        }

        private void m_rbfOffsetController_ValueChanged(object sender, EventArgs e)
        {
            double value = ((double)((NumericUpDown)sender).Value);
            m_rbfOffset = value;
            InitialiseRBFs();
            m_changed = true;
        }

        private void m_learningRateSelector_ValueChanged(object sender, EventArgs e)
        {
            double value = ((double)((NumericUpDown)sender).Value);
            Neuron.LearningRate = value;
            m_changed = true;
        }

        private void m_momentumController_ValueChanged(object sender, EventArgs e)
        {
            double value = ((double)((NumericUpDown)sender).Value);
            Neuron.MomentumAlpha = value;
            m_changed = true;
        }

        private void m_thresholdController_ValueChanged(object sender, EventArgs e)
        {
            double value = ((double)((NumericUpDown)sender).Value);
            m_stopThreshold = value;
            m_changed = true;
        }

        private void m_functionSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_function = ((ComboBox)sender).SelectedItem.ToString();
            InitialiseFunctionGraph();
            m_changed = true;
        }

        private void m_plotPointsController_ValueChanged(object sender, EventArgs e)
        {
            double value = ((double)((NumericUpDown)sender).Value);
            m_numPlotPoints = value;
            InitialiseInputPoints();
            m_changed = true;
        }

        private void m_noiseController_ValueChanged(object sender, EventArgs e)
        {
            double value = ((double)((NumericUpDown)sender).Value);
            m_noiseValue = value;
            InitialiseInputPoints();
            m_changed = true;
        }

        private void m_trainButton_Click(object sender, EventArgs e)
        {
            if (m_rbf != null)
            {
                if (RadialBasisFunctionNetwork.Running)
                {
                    RadialBasisFunctionNetwork.Running = false;
                    m_trainButton.Text = "Train";
                }
                else
                {
                    m_trainButton.Text = "Stop";
                    if (m_changed)
                    {
                        m_rbf.Initialise(1, m_rbfsData, 1);
                        m_changed = false;
                    }

                    RadialBasisFunctionNetwork.ErrorStopThreshold = m_stopThreshold;
                    List<List<double>> inputPatterns = new List<List<double>>();
                    List<List<double>> targetPatterns = new List<List<double>>();

                    for (int i = 0; i < m_inputs.Count; i++)
                    {
                        List<double> newInputPattern = new List<double>();
                        newInputPattern.Add(m_inputs[i]);
                        List<double> newTargetPattern = new List<double>();
                        newTargetPattern.Add(m_targets[i]);

                        inputPatterns.Add(newInputPattern);
                        targetPatterns.Add(newTargetPattern);

                    }

                    m_rbf.Train(inputPatterns, targetPatterns);
                }
            }
        }

        public void TestAndPresent()
        {
            List<double> finalData = new List<double>();

            for (double x = X_MIN; x < X_MAX; x += INTERVAL)
            {
                List<double> input = new List<double>();
                input.Add(x);

                finalData.AddRange(m_rbf.Test(input));
            }
            PlotNeuralOutput(finalData);
        }
        public void TestAndPresentAsync()
        {
            List<double> finalData = new List<double>();

            for (double x = X_MIN; x < X_MAX; x += INTERVAL)
            {
                List<double> input = new List<double>();
                input.Add(x);

                finalData.AddRange(m_rbf.Test(input));
            }
            PlotNeuralOutputAsync(finalData);
        }
        public void PlotNeuralOutput(List<double> output)
        {
            Series network = m_functionGraph.Series["Neural Network"];

            network.Points.Clear();
            network.ChartType = SeriesChartType.Line;
            network.Color = Color.Red;
            network.BorderWidth = 3;

            double x = 0;
            for (int i = 0; i < output.Count; i++)
            {
                network.Points.AddXY(x, output[i]);
                x += INTERVAL;
            }
        }
        public void PlotNeuralOutputAsync(List<double> output)
        {
            try
            {
                if (m_functionGraph.InvokeRequired)
                {
                    m_functionGraph.Invoke((MethodInvoker)delegate
                    {
                        Series network = m_functionGraph.Series["Neural Network"];

                        network.Points.Clear();
                        network.ChartType = SeriesChartType.Line;
                        network.Color = Color.Red;
                        network.BorderWidth = 3;

                        double x = 0;
                        for (int i = 0; i < output.Count; i++)
                        {
                            network.Points.AddXY(x, output[i]);
                            x += INTERVAL;
                        }
                    });
                }
            }
            catch (Exception) { }
        }
    }
}
